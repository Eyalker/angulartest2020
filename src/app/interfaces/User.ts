export interface User {
    uid: string,
    email?: string | null,
    photoUrl?: String,
    displayName?: string,
    
}
