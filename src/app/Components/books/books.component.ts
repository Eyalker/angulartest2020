
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorsService } from 'src/app/authors.service';
import { BooksService } from 'src/app/books.service';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  
  panelOpenState = false;
  books$:Observable<any>;
  constructor(private booksservice:BooksService) { }

  ngOnInit() {
   this.books$ = this.booksservice.getBooks();
   this.booksservice.addBooks();
  }



}