import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService:AuthService) { }
  
  userId:string;
  uemail:string;
  
  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.uemail = user.email;
       }
    )
  }

}
