import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth.service';
import { PostsService } from 'src/app/posts.service';

@Component({
  selector: 'app-postslist',
  templateUrl: './postslist.component.html',
  styleUrls: ['./postslist.component.css']
})
export class PostslistComponent implements OnInit {
  
  Posts$:Observable<any>;
  userId:string;
  like:number; 

  constructor(private postsservice:PostsService,public auth:AuthService) { }

  ngOnInit() {
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
      this.Posts$ = this.postsservice.getPosts(this.userId);
         }
      )
  }
  addLikes(id:string,likes:number){
    this.like = likes + 1 ; 
    this.postsservice.updateLikes(this.userId,id,this.like)
  }

  deletePost(key_id:string)
  {
  
    this.postsservice.deletePostFromCollection(this.userId,key_id);
  }
}