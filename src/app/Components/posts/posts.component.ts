
import { Component, OnInit } from '@angular/core';
// import { BlogPost } from './../interfaces/blog-post';
// import { User } from './../interfaces/user';
import { Observable } from 'rxjs';
import { PostsService } from 'src/app/posts.service';
import { Post } from 'src/app/interfaces/Post';
import { User } from 'src/app/interfaces/User';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  Posts$: Post[];
  Comments$: Comment[];
  userId:string;
  info:string;
  postId:number;
  
  constructor(private postsservice: PostsService ,public authService:AuthService) { }

  ngOnInit() {
    this.postsservice.getPost()
    .subscribe(data =>this.Posts$ = data );
    this.postsservice.getComment()
    .subscribe(data =>this.Comments$ = data );

    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }
    addPosts(title:string,body:string,id:number){
      this.postsservice.addPost(this.userId,title,body)
      this.postId = id;
      this.info = "The post uploaded"
  }
}