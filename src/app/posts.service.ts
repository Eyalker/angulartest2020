import { HttpClient , HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interfaces/Post';
import { User } from './interfaces/User';
import { Observable, throwError } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, catchError, map, flatMap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})


export class PostsService {
  private PostApi = "https://jsonplaceholder.typicode.com/posts" ;
  private CommentApi = "https://jsonplaceholder.typicode.com/comments" ; 
  
  constructor(private http: HttpClient,private db: AngularFirestore ,private authService:AuthService) { }


PostCollection:AngularFirestoreCollection;
UserCollection:AngularFirestoreCollection = this.db.collection('Users');


getPost(){
  return this.http.get<Post[]>(this.PostApi);
}

getComment(){
  return this.http.get<Comment[]>(this.CommentApi);
}

// Read
getPosts(userId:string): Observable<any[]> {
this.PostCollection = this.db.collection(`Users/${userId}/posts`);
console.log('Posts collection created');
return this.PostCollection.snapshotChanges().pipe(
  map(collection => collection.map(document => {
    const data = document.payload.doc.data();
    data.id = document.payload.doc.id;
    console.log(data);
    return data;
  }))
);    
} 

addPost(userId:string,title:string, body:string){
   const post = {title:title, body:body};
   this.UserCollection.doc(userId).collection('posts').add(post);
   alert('Saved For later Viewing');
  }

updateLikes(userId:string, id:string,like:number){
  this.db.doc(`Users/${userId}/posts/${id}`).update(
     {
       like:like
     }
   )
 }


 deletePostFromCollection(userID, key_id)
    {
      this.db.doc(`Users/${userID}/posts/${key_id}`).delete();
      
    }
}